﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour {

    public Camera cam;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;
    public GameObject Ojos;

    public sphereSensor leftSensor;
    public sphereSensor rightSensor;
    public sphereSensor stairsSensor; // stairs sensor

	// Use this for initialization
	void Start () {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        //get input
        input.GetInput();

        float currentVertical = input.vMovement;

        if ((leftSensor.Touch == true || rightSensor.Touch == true))
        {
            currentVertical = 0f;
            characterMovement.MoveCharacter(input.hMovement, currentVertical, cam, input.jump, input.dash, stairsSensor.Touch);
            
        }
        else
        {
            characterMovement.MoveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash, stairsSensor.Touch);
        }


        /*
        RaycastHit hit;

        if (Physics.Raycast(Ojos.transform.position, characterMovement.desiredMoveDirection, out hit, 5f))
        {
            Debug.Log(hit.collider.gameObject.tag);
            if (hit.collider.gameObject.CompareTag("Ground") && Input.GetKey(KeyCode.W))
            {
                Debug.Log(hit.collider.gameObject.name);
                //apply input
                characterMovement.MoveCharacter(0, 0, cam, input.jump, input.dash, sphereSensor.stairsUp);

            }
            else
            {
                //apply input
                characterMovement.MoveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);

            }
        }
        else
        {
            //apply input
            characterMovement.MoveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);

        }
        */

    }
}
